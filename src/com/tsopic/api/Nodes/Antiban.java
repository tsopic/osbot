package com.tsopic.api.Nodes;

import com.tsopic.api.Node;
import com.tsopic.api.util.Tree;
import com.tsopic.api.util.Walking;
import org.osbot.script.Script;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;

import java.awt.*;

/**
 * Created by T on 11.04.2014.
 */
public class Antiban extends Node {
    Walking walking;
    int[] treespot;
    public Antiban(Script script, Walking walking, int[] treeSpotID) {
        super(script);
        this.walking=walking;
        this.treespot=treeSpotID;
    }

    @Override
    public boolean validate() throws InterruptedException {
        return sA.myPlayer().isAnimating()&&!sA.myPlayer().isMoving();
    }

    @Override
    public boolean execute() throws InterruptedException {
        Walking w = new Walking(sA);
        sA.log("Antiban Started");

        if(sA.myPlayer().isAnimating()&&!sA.myPlayer().isMoving()){
            Tree tree = new Tree(sA, treespot);
            // If at treees
            int x = tree.getSecondNearest().getGridX();
            Rectangle y = tree.getSecondNearest().getMouseDestination().getDestination();
            sA.log(y.toString());
            sA.log("2nd tree xy  = ("+x+","+y+")");
            sA.client.moveMouse(new RectangleDestination(y), true);
            sA.log(Integer.toString(sA.myPlayer().getRotation()));

            if(tree.getNearestTree().getPosition().distance(sA.myPlayer().getPosition())<2&&sA.myPlayer().isAnimating()){
                sA.log("Moving mouse to next tree");
                sA.client.moveMouse(new RectangleDestination(y), true);
                sA.sleep(400);
                return true;
            }
        }
        return false;
    }

    public int randomWithRange(int min, int max)
    {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }
}
