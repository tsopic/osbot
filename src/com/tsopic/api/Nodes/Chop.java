package com.tsopic.api.Nodes;


/**
 * Created by T on 9.04.2014.
 */
 import org.osbot.script.rs2.Client;
 import com.tsopic.api.Node;
 import com.tsopic.api.util.Status;
 import com.tsopic.api.util.Tree;
 import com.tsopic.api.util.myPlayer;
 import groovy.ui.Console;
 import org.osbot.script.Script;
 import org.osbot.script.rs2.model.Entity;
 import org.osbot.script.rs2.model.RS2Object;


public class Chop extends Node {
    Status status;
    private int[] WILLOW_TREE_ID;
    public Chop(Script script, int[] WILLOW_TREE_ID) {
        super(script);
        this.WILLOW_TREE_ID=WILLOW_TREE_ID;
    }

    @Override
    public boolean validate() throws InterruptedException {
        myPlayer player = new myPlayer(sA);
        Tree tree = new Tree(sA, WILLOW_TREE_ID);
        return !sA.client.getInventory().isFull()&&tree.getNearestTree().isVisible()&&player.IsValid()&&tree.getNearestTree().exists();
    }


    @Override
    public boolean execute() throws InterruptedException {
        sA.log("Chopping Started");
        //status.setStatus("Chopping");
        Tree tree = new Tree(sA, WILLOW_TREE_ID);
        RS2Object currentTree = tree.getNearestTree();
        sA.log(currentTree.toString());
        try{
            if(tree.getNearestTree().exists()){
                sA.log("Tree Exists");
            }else{
                sA.log("Tree Does't Exist");
            }
            if (tree.isValid(tree.getNearestTree())) {
                sA.log("Tree is Valid");
                if(sA.canReach(currentTree)){
                    if(tree.getNearestTree().interact("Chop down")){
                        sA.sleep(400);
                        return true;
                    }
                }
            }
        }catch (NullPointerException e){
            sA.log(e.toString());
        }

        //Let the controller know that we fucked up


        return false;
    }
}
