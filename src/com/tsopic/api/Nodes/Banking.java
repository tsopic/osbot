package com.tsopic.api.Nodes;

import com.tsopic.api.Node;
import com.tsopic.api.util.Walking;
import org.osbot.script.Script;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.rs2.model.RS2Object;

import java.awt.*;
import java.awt.geom.Area;

/**
 * Created by T on 11.04.2014.
 */
public class Banking extends Node{

    private int[] BankBoothID;

    public Banking(Script script, int[] BANK_BOOTH_ID) {
        super(script);
        this.BankBoothID=BANK_BOOTH_ID;
    }

    @Override
    public boolean validate() throws InterruptedException {
        Walking walker = new Walking(sA);
        return sA.closestObjectForName("Bank booth").isVisible()&&sA.client.getInventory().getTotalItemsAmount()>1&&sA.closestObjectForName("Bank booth").getPosition().distance(sA.myPlayer().getPosition())<3;
    }

    @Override
    public boolean execute() throws InterruptedException {
        sA.log("Starting Banking");
        RS2Object bankbooth = sA.closestObject(BankBoothID);
        if(bankbooth.exists()){
            if(!sA.client.getBank().isOpen()){
                if(bankbooth.interact("Bank")){

                    sA.sleep((int) ((Math.random() * (700 - 600)) + 600));
                    if(sA.client.getBank().isOpen()){
                        if(sA.client.getBank().depositAll()){
                            return sA.client.getBank().close();
                        }
                    }
                    sA.sleep((int) (Math.random()*400-100+1));
                }
            }else{
                if(sA.client.getBank().depositAll()){
                    return sA.client.getBank().close();
                }
            }
        }
        return false;
    }
}
