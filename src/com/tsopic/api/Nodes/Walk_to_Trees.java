package com.tsopic.api.Nodes;

import com.tsopic.api.Node;
import com.tsopic.api.util.Tree;
import com.tsopic.api.util.Walking;
import com.tsopic.chopper.Willowchopper;
import org.osbot.script.Script;
import org.osbot.script.rs2.map.Position;
import sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl;

/**
 * Created by T on 11.04.2014.
 */
public class Walk_to_Trees extends Node{
    int[] treeID;
    Position[] pathToTrees;

    public Walk_to_Trees(Script script, int[] WILLOW_TREE_ID, Position[] pathToTrees) {
        super(script);
        treeID=WILLOW_TREE_ID;
        this.pathToTrees=pathToTrees;
    }

    @Override
    public boolean validate() throws InterruptedException {
        Tree tree = new Tree(sA, treeID);
        Walking w = new Walking(sA);
        return sA.client.getInventory().getTotalItemsAmount()<15&&!w.areaContainsMe("DRAYNOR_WILLOWS")&&!sA.myPlayer().isAnimating();
    }

    @Override
    public boolean execute() throws InterruptedException {
        sA.log("Walking To Trees");
        Walking walk = new Walking(sA, pathToTrees);
         if(walk.walkingWalkPath()){
             sA.sleep(400);
         }
        return true;
    }
}
