package com.tsopic.api.Nodes;

import com.tsopic.api.Node;
import com.tsopic.api.util.Status;
import com.tsopic.api.util.Tree;
import com.tsopic.api.util.Walking;
import com.tsopic.api.util.myPlayer;
import com.tsopic.chopper.Willowchopper;
import org.osbot.script.Script;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.ui.Bank;

/**
 * Created by T on 11.04.2014.
 */
public class Walk_to_bank extends Node {
    Status status;


    public Walk_to_bank(Script script) {
        super(script);
    }

    @Override
    public boolean validate() throws InterruptedException {
        Walking walker = new Walking(sA);
        RS2Object bankbooth = sA.closestObjectForName("Bank booth");
        return sA.client.getInventory().isFull()&&!sA.closestObjectForName("Bank booth").isVisible()&&!sA.myPlayer().isMoving();
    }

    @Override
    public boolean execute() throws InterruptedException {
        sA.log("Walking Started");
        Walking walker = new Walking(sA);
        //Walk to bank
        if(sA.client.getInventory().isFull()){
            //status.setStatus("Walking Bank");
            return walker.walkTo("DRAYNOR_BANK");
        }
        return false;
    }
}
