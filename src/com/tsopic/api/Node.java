package com.tsopic.api;

/**
 * Created by T on 9.04.2014.
 */
import org.osbot.script.Script;

public abstract class Node {
    public Script sA;
    public Node(Script script) {
        this.sA = script;
    }
    public abstract boolean validate() throws InterruptedException;
    public abstract boolean execute() throws InterruptedException;
}
