package com.tsopic.api.util;

import javafx.geometry.Pos;
import org.osbot.script.Script;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.utility.Area;

/**
 * Created by T on 11.04.2014.
 */
public class Walking {
    Script sA;
    Position[] path;
    private final Area DRAYNOR_WILLOWS = new Area(new Position[] {
            new Position(3080, 3239, 0),
            new Position(3097, 3239, 0),
            new Position(3097, 3224, 0),
            new Position(3086, 3224, 0)
    });

    private final Area DRAYNOR_BANK = new Area(new Position[] {
            new Position(3092, 3246, 0),
            new Position(3092, 3241, 0),
            new Position(3094, 3241, 0),
            new Position(3094, 3246, 0)
    });


    public Walking(Script script){
        this.sA=script;
    }

    public Walking(Script script, Position[] path){
        this.sA=script;
        this.path=path;
    }



    public Area getNamedArea(String areaname){
        if(areaname == "DRAYNOR_WILLOWS"){
            return DRAYNOR_WILLOWS;
        }else if(areaname == "DRAYNOR_BANK") {
            return DRAYNOR_BANK;
        }
        return new Area(0,0,0,0);
    }

    public boolean walkTo(String Areaname) throws InterruptedException {
        sA.log("Walking to " + Areaname);
        if(!areaContainsMe(Areaname)){
            if(sA.walkMiniMap(getNamedArea(Areaname).getRandomPosition(2))){
                return true;
            }
        }
        return false;
    }

    public boolean walkingWalkPath(Boolean a) throws InterruptedException {
        if(a){
            for(int i=0; i<path.length&&!sA.closestObjectForName("Willow").isVisible(); i++) {
                Position p = new Position(randoMizeX(path[i].getX()), randoMizeX(path[i].getY()), 0);
                if (!sA.myPlayer().isMoving() || (sA.myPlayer().getPosition().distance(p) > 5) && sA.canReach(p)) {
                    if (sA.canReach(p)) {
                        if (sA.walkMiniMap(p)) {
                            randomSleep();
                            sA.log("Walking somewhere");
                        }
                    }
                }
                return true;
            }
        }else{
            for(int i=path.length; i>path.length; i--){
                Position p = new Position(randoMizeX(path[i].getX()),randoMizeX(path[i].getY()),0);
                if(!sA.myPlayer().isMoving()||(sA.myPlayer().getPosition().distance(p)>5)&&sA.canReach(p)) {
                    if (sA.canReach(p)) {
                        if(sA.walkMiniMap(p)){
                            randomSleep();
                        };
                    }
                }
            }
        }
        return true;
    }
    public boolean walkingWalkPath() throws InterruptedException {
        Position a = getFarestReachable();
        for(int i=0; i<path.length&&!sA.closestObjectForName("Willow").isVisible(); i++){
            if(sA.walkMiniMap(getFarestReachableFromPath(a, true))){
                randomSleep();
                sA.log("trying to walk");
            }else{
                sA.log("failed to walk");
            }
        }
        return true;
    }

    private int randoMizeX(int x){
        return (int) Math.random()*1+1;

    }

    private Position getFarestReachable(){
        Position a = sA.myPlayer().getPosition();
        for(int i=1; i<path.length; i++){
            if(path[i].distance(sA.myPlayer().getPosition())>a.distance(sA.myPlayer().getPosition())){
                if(sA.canReach(path[i])){
                    a=path[i];
                }
            }
        }
        return a;
    }

    private Position getFarestReachableFromPath(Position a, Boolean traverse){
        Position b = a;
        if(traverse) {
            for (int i = findPathIndex(a); i < path.length; i++) {
                if (sA.canReach(path[i])) {
                    b = path[i];
                }
            }
        }else{
                for(int i=findPathIndex(a);i<0;i--) {
                    if (sA.canReach(path[i])) {
                        b = path[i];
                    }
                }
        }
        return  b;
    }

    private int findPathIndex(Position a){
        for(int i=0; i<path.length; i++){
            if(path[i].distance(a)<2){
                return i;
            }
        }
        return 0;
    }

    private void randomSleep() throws InterruptedException {
        sA.sleep((int) Math.random() * (400 - 100 + 1) + 100);
    }

    public boolean areaContainsMe(String area){
        Area a = getNamedArea(area);
        //sA.log(a.toString()+ "Contains me:" + a.contains(sA.myPosition()));
        return a.contains(sA.myPlayer().getPosition());
    }


    public void getDistanceToArea(String area){
        Area a = getNamedArea(area);
        //TODO
    }

    private void getNearestPosition(Area a){
        sA.log(Integer.toString(distanceBetween(a.getMaxX(), sA.myX())));
        //TODO
    }

    private int distanceBetween(int x, int y){
        if(x-y>0) {
            return x - y;
        }
        return -(x-y);
    }
}
