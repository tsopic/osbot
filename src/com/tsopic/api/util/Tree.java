package com.tsopic.api.util;

import com.tsopic.api.util.Modelpaint.paintModel;
import org.osbot.script.Script;
import org.osbot.script.rs2.model.RS2Object;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by T on 10.04.2014.
 */
public class Tree {
    int[] treeid;
    Script sA;
    public Tree(Script script, int[] treeid){
       this.sA=script;
       this.treeid=treeid;
    }

    public RS2Object getNearestTree(){
       // sA.log(sA.closestObject(treeid).toString());
        new paintModel(sA, sA.closestObject(treeid)).drawModel();

        return sA.closestObject(treeid);
    }

    public boolean isValid(RS2Object tree){
        return tree.exists()&&tree.isVisible();
    }

    public RS2Object getSecondNearest(){
        RS2Object a = sA.closestObject(treeid);
        List<RS2Object> objectlist = sA.closestObjectList(treeid);
        RS2Object b;
        if(objectlist.size()>0) {
            b = sA.closestObjectList(treeid).get(2);
            for (int i = 0; i < objectlist.size(); i++) {
               if (!a.getPosition().equals(objectlist.get(i).getPosition())) {
                   if (objectlist.get(i).getPosition().distance(sA.myPlayer().getPosition()) < b.getPosition().distance(sA.myPlayer().getPosition())) {
                       b = objectlist.get(i);
                   }
               }
            }
        }else{
            return a;
        }
        return b;
    }

    public int getTreeCount(){
        return sA.closestObjectList(treeid).size();
    }

}
