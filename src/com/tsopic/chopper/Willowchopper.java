package com.tsopic.chopper;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;

import com.tsopic.api.Nodes.*;
import com.tsopic.api.util.Status;
import com.tsopic.api.util.Walking;
import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import com.tsopic.api.Node;
import org.osbot.script.rs2.map.Position;

/**
 * Created by T on 9.04.2014.
 */
@ScriptManifest(author = "Tsopic", info = "Chops willows", name = "Willowchopper", version = 0.1)
public class Willowchopper extends Script {
    public static ArrayList<Node> nodes = new ArrayList<>();
    private final int[] WILLOW_TREE_ID={13422,13426, 13430};
    Status status;
    private final String treeSpotName="DRAYNOR_WILLOW";
    private final int[] BANK_BOOTH_ID={2100};
    //PATH TO TREE;
    private final Position[] DRYNOR_BANK_WILLOW_PATH = new Position[] {
            new Position(3085, 3247, 0),
            new Position(3085, 3242, 0),
            new Position(3085, 3237, 0),
            new Position(3085, 3238, 0)
    };

    @Override
    public void onExit() throws InterruptedException {
        // TODO Auto-generated method stub
        super.onExit();
    }

    @Override
    public int onLoop() throws InterruptedException {
        for (final Node node : nodes) {    //Go threw all of our nodes
            if (node.validate()) {     //Validate each one
                node.execute();    //Good to go, execute it
                sleep(500);        //Wait 500ms inbetween each node.
            }
        }
        return 10;
    }

    @Override
    public void onPaint(Graphics arg0) {
        super.onPaint(arg0);
    }

    @Override
    public void onStart() {
        //Add in our two nodes, passing this as reference so they can access it.
        //client.setMouseSpeed((int) (Math.random()*((200-100)+1)+100));
        client.setMouseSpeed(7);
        Collections.addAll(nodes,
                new Chop(this, WILLOW_TREE_ID),
                new Walk_to_bank(this),
                new Banking(this, BANK_BOOTH_ID),
                new Walk_to_Trees(this, WILLOW_TREE_ID, DRYNOR_BANK_WILLOW_PATH),
                new Antiban(this, new Walking(this), WILLOW_TREE_ID)
                );
        super.onStart();
    }




}